package otp

import "testing"

func TestHOTP(t *testing.T) {
	secret := []byte("12345678901234567890")
	expectedRes := []string{
		"755224",
		"287082",
		"359152",
		"969429",
		"338314",
	}

	for i, v := range expectedRes {
		res, err := HOTP(secret, uint64(i), 6)
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		if res != v {
			t.Errorf("Incorrect hotp %s, should be %s", res, v)
			t.Fail()
		}
	}
}
