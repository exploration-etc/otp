package otp

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/binary"
	"fmt"
	"math"
)

// HOTP returns otp based on increment and length
// ref: https://tools.ietf.org/html/rfc4226
func HOTP(secret []byte, counter uint64, length int) (string, error) {

	counterSlice := make([]byte, 8)
	binary.BigEndian.PutUint64(counterSlice, counter)
	mac, err := GenerateHMAC(secret, counterSlice)

	if err != nil {
		return "", err
	}

	// take the least significant 4 bits as the byte offset
	offSet := mac[19] & 0x0F

	return fmt.Sprintf("%0*d", length, extract31Bits(mac, offSet)%int32(math.Pow10(length))), nil
}

// GenerateHMAC applies hmacsecret
func GenerateHMAC(key []byte, counter []byte) ([]byte, error) {

	mac := hmac.New(sha1.New, key)
	_, err := mac.Write(counter)
	if err != nil {
		return nil, err
	}

	return mac.Sum(nil), nil

}
func extract31Bits(mac []byte, offSet byte) int32 {
	var result int32
	result = 0
	result |= int32(mac[int(offSet)]&0x7f) << 24
	result |= int32(mac[int(offSet)+1]) << 16
	result |= int32(mac[int(offSet)+2]) << 8
	result |= int32(mac[int(offSet)+3])
	return result
}
