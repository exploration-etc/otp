package main

import (
	"encoding/base32"
	"flag"
	"log"
	"otp/pkg/otp"
)

func main() {
	secret := flag.String("secret", "", "base32 encoded secret (required)")
	length := flag.Int64("length", 6, "length of otp code to generate [6,8]")
	counter := flag.Int64("counter", 0, "current counter value")
	flag.Parse()

	if *secret == "" {
		log.Fatal("-secret is required, please check --help")
	}

	secretBytes, err := base32.StdEncoding.DecodeString(*secret)
	if err != nil {
		log.Fatalf("Failed to parse secret %s, make sure it is a valid base32 string: \n%s\n", *secret, err)
	}

	res, err := otp.HOTP(secretBytes, uint64(*counter), int(*length))
	if err != nil {
		log.Fatalf("Failed to generate otp: \n%s\n", err)
	}

	log.Printf("Successfully Generated OTP: %s", res)
}
